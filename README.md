## Pré-requisitos

-   PHP 7.0+
-   NGINX
-   Composer

Para criar o ambiente de desenvolvimento na sua maquina, basta abrir o terminal e seguir os seguintes comandos:

```
$ git clone https://mm-silva@bitbucket.org/mm-silva/petapi.git
$ cd petapi/
cp .env.example .env
```

Podemos usar qualquer meio para montar o ambiente, mas nesse caso vamos usar o laradock dentro do nosso projeto:

```
$ git clone https://github.com/laradock/laradock.git
$ cd laradock
$ cp .env.example .env
```

Defina as portas caso já esteja usando as portas padrões, abra e edite o .env do laradock:

```
NGINX_HOST_HTTP_PORT=80
```

Também altere a porta do banco de dados caso não queira usar algum banco externo, defina o nome da database:

```
MARIADB_VERSION=latest
MARIADB_DATABASE=teste
MARIADB_USER=default
MARIADB_PASSWORD=secret
MARIADB_PORT=3306
MARIADB_ROOT_PASSWORD=root
```

Depois de salvar as alterações, ainda nesta pasta, crie o ambiente:

```
$ docker-compose up -d nginx mariadb
```

Se tudo der certo, continue:

```
$ docker-compose exec workspace bash
$ composer install
$ php artisan migrate
```
