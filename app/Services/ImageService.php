<?php
namespace App\Services;

use Aws\S3\S3Client;
use Illuminate\Http\Request;
use Guzzle\Http\EntityBody;

class ImageService {


 public static function up(Request $request,$name, $folder, $file){

     if(isset($request->photo)) {

         $file_name =  ($folder !== null ? $folder.'/' . $file . "." . $request->file($name)->getClientOriginalExtension() : $file . "." . $request->file($name)->getClientOriginalExtension());
         $temp_file_location = $request->file($name)->getPathName();


         $s3 = new S3Client([
             'region' => env("AWS_DEFAULT_REGION"),
             'version' => 'latest',
             'credentials' => [
                 'key' => env("AWS_ACCESS_KEY_ID"),
                 'secret' => env("AWS_SECRET_ACCESS_KEY")]
         ]);


          $result = $s3->putObject([
             'Bucket' => env("AWS_BUCKET"),
             'Key' => $file_name,
             'SourceFile' => $temp_file_location
         ]);

          return ["path" => $file_name];
        }

     }

     public static function show($name, $folder){
//         $s3 = new S3Client([
//             'region' => env("AWS_DEFAULT_REGION"),
//             'version' => 'latest',
//             'credentials' => [
//                 'key' => env("AWS_ACCESS_KEY_ID"),
//                 'secret' => env("AWS_SECRET_ACCESS_KEY")]
//         ]);
//         $path = "$folder/$name";
//         $response = $s3->doesObjectExist(env("AWS_BUCKET"), "$folder/$name");
//
//
//             $result = $s3->getObject(array(
//                 'Bucket' => env("AWS_BUCKET"),
//                 'Key' => $path
//             ));

             return response()->json(['image' => "https://" . env('AWS_BUCKET') . env("AWS_DEFAULT_REGION") . "amazonaws.com/" . ($folder !== null ? $folder."/".$name : $name)]);


     }
}
