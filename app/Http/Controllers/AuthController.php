<?php

namespace App\Http\Controllers;

use App\Models\UsersAuthentication;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Aws\S3\S3Client;
use Carbon\Carbon;
use App\Models\Users;
use App\Models\UsersAddress;
use App\Models\UsersPhotos;

class AuthController extends Controller
{
    /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function login(Request $request)
    {
        //validate incoming request
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        // $credentials = $request->only(['email', 'password']);

        // if (! $token = Auth::attempt($credentials)) {
        //     return response()->json(['message' => 'Unauthorized'], 401);
        // }

        $credentials = [
            'username' => $request['username'],
            'password' => $request['password'],
        ];
    
        // Dump data
        dd($credentials);
    
        if (!$token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function register(Request $request)
    {

        if (!empty($request->json()->all())) {
            $json = $request->getContent();
            $request->request->add(json_decode($json, true));
        }
        //validate incoming request
        $this->validate($request, [
            'firstName' => 'required|string',
            'email' => 'required|email|unique:UsersAuthentication',
            'password' => 'required|confirmed',
        ]);

        $userDescription = [
            'document' => $request->document,
            'dateBirth' => $request->dateBirth,
            'gender' => $request->gender,
            'firstName' => $request->firstName,
            'lastName' => $request->lastName,
            'telefone' => $request->telefone,
            'ong' => $request->ong
        ];


          $userAddress = [
            'address' => !isset($request->address) ? "-" : $request->address,
            'number' => $request->number,
            'neighborhood' => $request->neighborhood,
            'city' => $request->city,
            'state' => $request->state,
            'postalCode' => $request->postalCode,
            'country' => $request->country
        ];

        try{

            echo 'create';

            $addressId = UsersAddress::create($userAddress)->id;
            $userDescription["addressId"] = $addressId;
            $userId = Users::create($userDescription)->id;
            $img = ImageService::up($request, 'photo',"./", $userId);

            $userPhoto = [
                'desc' => !isset($request->desc) ? '-' : $request->desc,
                'bucketName' => env("AWS_BUCKET"),
                'userId' => "$userId",
                'key' => $img['path']
            ];
            $photoId = UsersPhotos::create($userPhoto)->id;

            echo 'Photo';

            $userAuthentication = [
                'email' => $request->input('email'),
                'password' => app('hash')->make($request->input('password')),
                'userId' => $userId,
                'passwordChange' => false,
                'type' => "pethelp"
            ];
            $authId = UsersAuthentication::create($userAuthentication)->id;

            echo 'Authentication';

            // $register = new UsersAuthentication();
            // $register->email = $request->input('email');
            // $register->password = app('hash')->make($request->input('password'));
            // $register->userId = $userId;
            // $register->passwordChange = false;
            // $register->type = "pethelp";
            // $register->save();

          $select = DB::select(" SELECT a.authId, u.firstName, u.lastName, a.email, p.key, p.desc FROM UsersAuthentication a
                    inner JOIN Users u ON a.userId = u.userId inner join UsersPhotos p on u.userId = p.userId
                     where u.userId = $userId");

            return response()->json(['user' => $select, 'message' => 'CREATED'], 201);

        } catch (Exception $e) {
            //return error message
            return $e->getMessage();;
        }


    }

    public function logout () {
        Auth::logout();
        return response()->json(['message' => 'Successfully logged out']);
    }
}
