<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Users extends Model {

    public $table = "Users";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $fillable = [
        'userId','addressId', 'document', 'dateBirth', 'gender','firstName','lastName','telefone','ong','createdAt','updatedAt'
    ];


}
