<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersPhotos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UsersPhotos', function (Blueprint $table)
        {
            $table->increments('photoId');
            $table->string('key');
            $table->string('desc');
            $table->string('bucketName', 20);
            $table->integer('userId')->unsigned();
            $table->foreign('userId')->references('userId')->on('Users');
            $table->dateTime('created_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('UsersPhotos');
    }
}
