<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersAuthenticationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UsersAuthentication', function (Blueprint $table)
        {
	    $table->increments('authId');
            $table->string('email');
            $table->text('password');
            $table->integer('userId')->unsigned();
            $table->foreign('userId')->references('userId')->on('Users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('UsersAuthentication');
    }
}
