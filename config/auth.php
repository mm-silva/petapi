<?php

return [
    'defaults' => [
        'guard' => 'api',
        'passwords' => 'UsersAuthentication',
    ],

    'guards' => [
        'api' => [
            'driver' => 'jwt',
            'provider' => 'UsersAuthentication',
        ],
    ],

    'providers' => [
        'UsersAuthentication' => [
            'driver' => 'eloquent',
            'model' => \App\Models\UsersAuthentication::class
        ]
    ]
];
